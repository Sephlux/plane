﻿using MbientLab.MetaWear.Impl.Platform;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static SensorManager;

namespace MbientLab.MetaWear.Android {
    class BluetoothLeGatt : IBluetoothLeGatt {
        // Implementation here
        public ulong BluetoothAddress { get; private set; }
        public string Mac { get; private set; }

        private TaskCompletionSource<bool> DcTaskSource;

        private HashSet<string> serviceList;

        public BluetoothLeGatt(string mac, string hci = null) {
            Mac = mac;
            BluetoothAddress = Convert.ToUInt64(mac.Replace(":", ""), 16);
        }

        public Action OnDisconnect { get; set; }

        public SensorManager SensorManager { get; set; }

        public async Task DisconnectAsync() {
            DcTaskSource = new TaskCompletionSource<bool>();
            BluetoothLEHardwareInterface.DisconnectPeripheral(Mac, (name) => { DcTaskSource.SetResult(true); });
            await DcTaskSource.Task;
        }

        public async Task DiscoverServicesAsync() {
            var task = new TaskCompletionSource<Tuple<string, string, string>>();

            Action<string, string, string> callback = (address, serviceUUID, characteristicUUID) => {
                task.SetResult(new Tuple<string, string, string>(address, serviceUUID, characteristicUUID));
            };
            
            BluetoothLEHardwareInterface.ConnectToPeripheral(Mac,
                (address) => {
                    serviceList = new HashSet<string>();
                },
                (address, serviceUUID) => {
                    serviceList.Add(serviceUUID);
                },
                callback, (disconnectAddress) => {
                    Debug.Log("LeGattDisconnect");
                    SensorManager.OnDisconnect();
                });
            Tuple<string, string, string> result = await task.Task;

        }

        public async Task EnableNotificationsAsync(Tuple<Guid, Guid> gattChar, Action<byte[]> handler) {

            var task = new TaskCompletionSource<string>();
            
            Action<string, string, byte[]> callback = (name, characteristic, val) => {
                handler(val);
            };
            Action<string, string> notification = (name, characteristic) => {
                task.SetResult(null);
            };

            BluetoothLEHardwareInterface.SubscribeCharacteristicWithDeviceAddress(Mac, gattChar.Item1.ToString(), gattChar.Item2.ToString(), notification, callback);

            await task.Task;

        }

        public async Task<bool> ServiceExistsAsync(Guid serviceGuid) {

            if (serviceList != null) {
                if (serviceList.Contains(serviceGuid.ToString())) {
                    await Task.FromResult(true);
                    return true;
                } else {
                    await Task.FromResult(false);
                    return false;
                }
            } else {
                await Task.FromResult(false);
                return false;
            }
        }

        public async Task<byte[]> ReadCharacteristicAsync(Tuple<Guid, Guid> gattChar) {
            var task = new TaskCompletionSource<byte[]>();

            Action<string, byte[]> callback = (characteristic, value) => {
                task.SetResult(value);
            };

            BluetoothLEHardwareInterface.ReadCharacteristic(Mac, gattChar.Item1.ToString(), gattChar.Item2.ToString(), callback);
            var res = await task.Task;
            return res;
        }

        public async Task WriteCharacteristicAsync(Tuple<Guid, Guid> gattChar, GattCharWriteType writeType, byte[] value) {

            var task = new TaskCompletionSource<string>();

            Action<string> callback = (message) => {
                task.SetResult(message);
            };

            BluetoothLEHardwareInterface.WriteCharacteristic(Mac, gattChar.Item1.ToString(), gattChar.Item2.ToString(), value, value.Length, true, callback);

            var res = await task.Task;
        }
    }

    class IO : ILibraryIO {

        private readonly string MacAddr;

        public IO(string macAddr) {
            MacAddr = macAddr.Replace(":", "");
        }

        public async Task<Stream> LocalLoadAsync(string key) {
            string path = Application.persistentDataPath + "/sensor" + MacAddr + key + ".dat";
            return await Task.FromResult(File.Open(path, FileMode.Open));
        }

        public Task LocalSaveAsync(string key, byte[] data) {

            string path = Application.persistentDataPath + "/sensor" + MacAddr + key + ".dat";

            using (Stream outs = File.Open(path, FileMode.Create)) {
                outs.Write(data, 0, data.Length);
            }
            return Task.CompletedTask;
        }
    }
}