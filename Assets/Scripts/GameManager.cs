﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Threading;

public class GameManager : MonoBehaviour {

    [SerializeField]
    private GameObject birdStartPosition;
    [SerializeField]
    private GameObject birdPrefab;
    [SerializeField]
    private GameObject[] powerUpPrefabs;
    [SerializeField]
    private GameObject hpObject;
    [SerializeField]
    private GameObject gameOverUI;
    [SerializeField]
    private GameObject plane;
    [SerializeField]
    private Text score;
    [SerializeField]
    private GameObject backgroundImage;
    [SerializeField]
    private GameObject levelData;
    [SerializeField]
    private GameObject powerUpTimer;
    [SerializeField]
    private GameObject powerUpContainer;

    private bool gameOver = false;
    private float instantiateTime = 5.0f;
    private float instantiateTimePowerUp = 8.0f;
    private float deltaTime = 0.0f;
    private float deltaTimePowerUp = 0.0f;
    private Vector3 wrld;
    private float half_sz = 0.5f;
    private SettingsData data;
    private DateTime startTime;
    private TimeSpan ts;
    private bool birdFlag = false;
    private float highscore = 0;
    private float scoreDelta = 0.005f;
    private List<float> yPosition;
    private int birdCount = 0;
    private int birdDirectInterval = 3;
    private List<GameObject> powerUps;
    private List<PowerUp> currentPowerUps;

    public bool GameOver { get => this.gameOver; 
        set {
            this.gameOver = value;
            if (value == true) {
                gameOverUI.gameObject.SetActive(true);
                ts = DateTime.UtcNow - startTime;
                gameOverUI.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = "Time: " + ts.Minutes.ToString() + "m " + ts.Seconds.ToString() + "s";
                gameOverUI.transform.GetChild(0).transform.GetChild(2).GetComponent<Text>().text = "Score: " + ((int)Highscore).ToString();
                if(data.playedGames == null) {
                    data.playedGames = new List<SaveGame>();
                }
                data.playedGames.Add(new SaveGame((int)highscore, ts.Seconds, ts.Minutes, data.gameSpeed, data.planeSpeed, data.birdInterval, yPosition, DateTime.Now, data.from, data.to, data.powerUpInterval));
                if (data.maxScore < highscore) {
                    data.maxScore = highscore;
                }
                DataManager.Save(data);
            }
        }
    }

    public float Highscore {
        get => this.highscore;
        set {
            this.highscore = value;
            score.text = "Score: <color=\"lime\">" + ((int)this.highscore).ToString() + "</color>";
        }
    }

    public List<GameObject> PowerUps { get => this.powerUps; set => this.powerUps = value; }

    void Start() {
        Highscore = 0;
        yPosition = new List<float>();
        data = DataManager.Load();
        startTime = DateTime.UtcNow;
        deltaTime = data.birdInterval - 1;
        instantiateTime = data.birdInterval;
        instantiateTimePowerUp = data.powerUpInterval;
        wrld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f));
        half_sz = birdPrefab.GetComponent<Collider2D>().bounds.size.y;
        powerUps = new List<GameObject>();

        plane.GetComponent<Animator>().SetLayerWeight(0, 0);
        plane.GetComponent<Animator>().SetLayerWeight(1, 0);
        plane.GetComponent<Animator>().SetLayerWeight(2, 0);
        plane.GetComponent<Animator>().SetLayerWeight((int)data.gameLevel, 1);
        backgroundImage.GetComponent<SpriteRenderer>().sprite = levelData.GetComponent<Level>().LevelBackground[(int)data.gameLevel];

    }

    void Update() {
        if (!GameOver) {
            deltaTime += Time.deltaTime;
            deltaTimePowerUp += Time.deltaTime;
            if (deltaTime >= instantiateTime) {
                deltaTime = 0;
                instantiateBird();
            } else if (deltaTimePowerUp >= instantiateTimePowerUp) {
                deltaTimePowerUp = 0;
                instantiatePowerUp();
            }
        }
    }

    void FixedUpdate() {
        if (!GameOver) {
            Highscore += scoreDelta*data.gameSpeed;
        }
    }

    private void clearAnimationWeights(GameObject obj) {
        obj.GetComponent<Animator>().SetLayerWeight(1, 0);
        obj.GetComponent<Animator>().SetLayerWeight(2, 0);
        obj.GetComponent<Animator>().SetLayerWeight(3, 0);
        obj.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight(1, 0);
        obj.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight(2, 0);
        obj.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight(3, 0);
    }

    public void instantiateBird() {
        GameObject obj = Instantiate(birdPrefab, new Vector3(birdStartPosition.transform.position.x, UnityEngine.Random.Range(wrld.y + 1, -wrld.y - 1), 0.0f), Quaternion.identity);
        obj.GetComponent<BirdController>().GameManager = this;
        obj.GetComponent<BirdController>().PlaneContoller = plane.GetComponent<PlaneContoller>();
        obj.GetComponent<BirdController>().Speed = data.gameSpeed;
        clearAnimationWeights(obj);
        obj.GetComponent<Animator>().SetLayerWeight((int)data.gameLevel+1, 1);
        obj.transform.GetChild(0).GetComponent<Animator>().SetLayerWeight((int)data.gameLevel+1, 1);
        /*if (birdFlag) {
            obj.transform.GetChild(0).GetChild(0).GetComponent<PolygonCollider2D>().isTrigger = false;
            obj.transform.GetChild(0).GetChild(1).GetComponent<PolygonCollider2D>().isTrigger = false;
            obj.transform.GetChild(0).GetChild(2).GetComponent<PolygonCollider2D>().isTrigger = false;
        }*/
        if (birdCount % birdDirectInterval == 0) {
            Vector3 pos = obj.transform.position;
            pos.y = plane.transform.position.y;
            obj.transform.position = pos;
        }
        birdCount++;
        foreach (GameObject heart in powerUps) {
            Physics2D.IgnoreCollision(obj.GetComponent<PolygonCollider2D>(), heart.GetComponent<PolygonCollider2D>());
        }
    }

    public void instantiatePowerUp() {
        int rand = UnityEngine.Random.Range(0, powerUpPrefabs.Length);
        GameObject obj = Instantiate(powerUpPrefabs[rand], new Vector3(birdStartPosition.transform.position.x, UnityEngine.Random.Range(wrld.y + half_sz, -wrld.y - half_sz), 0.0f), Quaternion.identity);
        obj.GetComponent<PowerUpController>().GameManager = this;
        obj.GetComponent<PowerUpController>().Plane = plane;
        obj.GetComponent<PowerUpController>().Speed = data.gameSpeed/3;
        if (birdFlag) {
            obj.GetComponent<PolygonCollider2D>().isTrigger = false;
        }
        powerUps.Add(obj);
    }

    public void LooseHP(int hp) {
        Color col = hpObject.transform.GetChild(hp-1).GetComponent<Image>().color;
        col.a = 0.5f;
        hpObject.transform.GetChild(hp-1).GetComponent<Image>().color = col;
        if (3-hp <= 1) {
            birdFlag = true;
        }
    }

    public void SetPosition(Transform t) {
        yPosition.Add(t.position.y);
    }

    public void AddHP(int hp) {
        Color col = hpObject.transform.GetChild(hp).GetComponent<Image>().color;
        col.a = 1f;
        hpObject.transform.GetChild(hp).GetComponent<Image>().color = col;
        birdFlag = false;
    }

    public void IncreaseHighscore () {
        Highscore += 100;
    }

    public void OnPlayAgain() {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void OnExit() {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }

    public void OnHome() {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }

    public void AddPowerUpTimer(PowerUp powerUp, float time) {
        GameObject[] powerUpTimers = GameObject.FindGameObjectsWithTag("PowerUpTimer");
        currentPowerUps = new List<PowerUp>();
        foreach (GameObject timer in powerUpTimers) {
            currentPowerUps.Add(timer.GetComponent<PowerUpTimer>().PowerUp);
        }
        if (!currentPowerUps.Contains(powerUp)) {
            GameObject obj = Instantiate(powerUpTimer); //TODO: scale changes automatically
            obj.transform.SetParent(this.powerUpContainer.transform, true);
            obj.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            obj.GetComponent<PowerUpTimer>().Timer = time;
            obj.GetComponent<PowerUpTimer>().PowerUp = powerUp;
            obj.GetComponent<PowerUpTimer>().PowerUpIcon = this.powerUpPrefabs[(int)powerUp]
                                                               .GetComponent<SpriteRenderer>().sprite;
        } else {
            foreach (GameObject timer in powerUpTimers) {
                timer.GetComponent<PowerUpTimer>().UpdateTime(powerUp);
            }
        }

    }
    public void RemovePowerUpTimer (PowerUp powerUp) {
        GameObject[] powerUpTimers = GameObject.FindGameObjectsWithTag("PowerUpTimer");
        foreach (GameObject timer in powerUpTimers) {
           if (timer.GetComponent<PowerUpTimer>().PowerUp == powerUp) {
                Destroy(timer);
           }
        }
    }

}
 