﻿using UnityEngine;
using System.Collections;

public class BGScroller : MonoBehaviour {
    public float scrollSpeed = 2.0f;
    public float tileSizeZ = 50000.0f;
    public GameManager gameManager;

    private Vector3 startPosition;

    void Start() {
        startPosition = transform.position;
    }

    void Update() {
        if (!gameManager.GameOver) {
            float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ);
            transform.position = startPosition + Vector3.left * newPosition;
        }
    }
}
