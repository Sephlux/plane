﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using MbientLab.MetaWear.Impl;
using MbientLab.MetaWear.Android;
using MbientLab.MetaWear.Sensor;
using System.Collections;
using MbientLab.MetaWear.Data;
using System.Runtime.InteropServices;
using MbientLab.MetaWear.Peripheral;
using UnityEngine.Assertions.Must;
using MbientLab.MetaWear.Core;
using System.Threading;
using MbientLab.MetaWear.Sensor.GyroBmi160;
using MbientLab.MetaWear.Core.SensorFusionBosch;
//namespace MetaWear.NETCore {
public class SensorManager : MonoBehaviour {

    /**
     *  2020-06-06 12:56:45.496 13726-13750/com.youngdevelopment.planeGame I/UnityBluetoothLE: LocalLoadAsync
        2020-06-06 12:56:45.504 13726-13750/com.youngdevelopment.planeGame I/UnityBluetoothLE: DiscoverServicesAsync
        2020-06-06 12:56:45.504 13726-13750/com.youngdevelopment.planeGame I/UnityBluetoothLE: ServiceExistsAsync
        2020-06-06 12:56:45.521 13726-13750/com.youngdevelopment.planeGame I/UnityBluetoothLE: ReadCharacteristicAsync
     * 
     */

    private string DeviceName = "MetaWear";


    public Text Values;
    public Text StatusText;
    public Text connectText;
    public GameObject foot;
    public Button startButton;
    public Button setValueButton;
    public Button connectButton;
    public InputField fromInput;
    public InputField toInput;
    public SettingsManager settingsManager;

    public enum States {
        None,
        Scan,
        Connect,
        Disconnect,
        Disconnecting,
        ConfigureSensorFusion,
        ReadSensorFusion
    }

    private MetaWearBoard metawear;
    private float _timeout = 0f;
    private string _deviceAddress;
    private States _state = States.None;
    private ISensorFusionBosch sensorFusion;
    private bool readingData = false;

    private float minValue = float.MaxValue;
    private float maxValue = float.MinValue;
    private float currentValue;
    private bool started = false;
    private bool connection = false;
    private bool neutralSet = false;

    private string statusMessage;

    public string StatusMessage {
        set {
            statusMessage = value;
            if (StatusText != null) {
                StatusText.text = "Status: " + statusMessage;
            }
        }
        get {
            return statusMessage;
        }

    }

    public float CurrentValue { get => this.currentValue; 
        set {
            if (started && Values != null) {
                this.currentValue = value;
                if (!neutralSet) {
                    settingsManager.Neutral = this.currentValue;
                    neutralSet = true;
                }
                if (value < minValue) {
                    minValue = value;
                }
                if (value > maxValue) {
                    maxValue = value;
                }

                Values.text = "Min: " + Math.Round(minValue, 2).ToString() + "°\nMax: " + Math.Round(maxValue, 2).ToString() + "°\nCurrent: " + Math.Round(currentValue, 2).ToString() + "°";
                foot.transform.eulerAngles = new Vector3(foot.transform.eulerAngles.x, foot.transform.eulerAngles.y, -currentValue);
            } 
            if (GameRunning) {
                this.currentValue = value;
            }
            
            //else {
                //Values.text = "Min: 0.00°\nMax: 0.00°\nCurrent: 0.00°";
                //foot.transform.eulerAngles = new Vector3(foot.transform.eulerAngles.x, foot.transform.eulerAngles.y, 0.0f);
                //minValue = 0f;
                //maxValue = 0f;
            //}
        }
    }

    public bool GameRunning { get; set; } = false;
    public States State { get => this._state; set => this._state = value; }

    public void GoBack() {
        SceneManager.LoadScene("SettingsScene", LoadSceneMode.Single);
    }

    public void Connect() {
        if (!connection) {
            connection = !connection;
            SetState(States.Scan, 0.5f);
            StatusMessage = "Starting...";
            //connectText.text = "Disconnect";
            if (connectButton != null) {
                connectButton.interactable = false;
            }
        } else {
            SetState(States.Disconnect, 0.5f);
            //connectText.text = "Connect";
            if (connectButton != null) {
                connectButton.interactable = false;
            }
        }
    }

    public void SetValues() {
        fromInput.text = Math.Round(minValue, 2).ToString();
        toInput.text = Math.Round(maxValue, 2).ToString();
    }

    public void StartTest() {
        started = !started;
        if (started) {
            Values.text = "Min: 0.00°\nMax: 0.00°\nCurrent: 0.00°";
            minValue = float.MaxValue;
            maxValue = float.MinValue;
        } else {
            neutralSet = false;
        }
    }

    void Reset() {
        _state = States.None;
        StatusMessage = "";
    }

    public void  SetState(States newState, float timeout) {
        if (newState == States.Scan && !connection) {
            
        } else {
            _state = newState;
        }
        _timeout = timeout;
    }
    void StartProcess() {
        Reset();
        BluetoothLEHardwareInterface.Initialize(true, false, () => {

            SetState(States.None, 0.1f);
            Connect();
        }, (error) => {
            BluetoothLEHardwareInterface.Log("Error: " + error);
        });
    }

    void Awake() {
        DontDestroyOnLoad(this.transform.gameObject);
    }

    void Start() {
        Reset();
        StartProcess();
    }


    void Update() {
        if (_timeout > 0f) {
            _timeout -= Time.deltaTime;
            if (_timeout <= 0f) {
                _timeout = 0f;
                switch (_state) {
                    case States.None:
                        //connectButton.interactable = true;
                        StatusMessage = StatusMessage;
                        break;
                    case States.Scan:
                        StatusMessage = "Scan for Sensor...";
                        //if (!scanning) {
                        //scanning = true;
                        BluetoothLEHardwareInterface.ScanForPeripheralsWithServices(null, (address, deviceName) => {
                            if (deviceName.Contains(DeviceName)) {
                                BluetoothLEHardwareInterface.StopScan();
                                _deviceAddress = address;
                                StatusMessage = "Found Sensor...";
                                SetState(States.Connect, 0.5f);
                            }
                        }, null, true);
                            //scanning = false;
                        //}
                        break;
                    case States.Connect:
                        StatusMessage = "Connect to sensor...";
                        BluetoothLeGatt b = new BluetoothLeGatt(_deviceAddress) {
                            SensorManager = this
                        };
                        metawear = new MetaWearBoard(b, new IO(_deviceAddress));
                        ConnectAsync();
                        SetState(States.None, 0.5f);
                        break;
                    case States.ConfigureSensorFusion:
                        StatusMessage = "Configure sensor...";
                        sensorFusion = metawear.GetModule<ISensorFusionBosch>();
                        sensorFusion.Configure();
                        ConfigureSensorFusionAsync();
                        break;
                    case States.ReadSensorFusion:
                        StatusMessage = "Connected...";      
                        if (connectButton != null) {
                            connectText.text = "Disconnect";
                            connectButton.interactable = true;
                        }
                        if (startButton != null) {
                            startButton.interactable = true;
                        }
                        if (setValueButton != null) {
                            setValueButton.interactable = true;
                        }
                        readingData = true;
                        ReadSensorFusionAsync();
                        break;
                    case States.Disconnect:
                        StatusMessage = "Disconnecting...";
                        DisconnectAsync();
                        break;
                }
            }
        }
    }

    private async void DisconnectAsync() {
        connection = !connection;
        await metawear.DisconnectAsync();
        StatusMessage = "Disconnected...";
        if (startButton != null) {
            startButton.interactable = false;
        }
        if (setValueButton != null) {
            setValueButton.interactable = false;
        }
        if (connectButton != null) {
            connectButton.interactable = true;
            connectText.text = "Connect";
        }
        SetState(States.None, 0.5f);
    }

    private async void ReadSensorFusionAsync() {
        await sensorFusion.EulerAngles.AddRouteAsync(source =>
                source.Stream(data => {
                    EulerAngles euler = data.Value<EulerAngles>();
                    CurrentValue = euler.Pitch;
                    StatusMessage = "Connected...";
                    if (connectButton != null) {
                        connectText.text = "Disconnect";
                        connectButton.interactable = true;
                    }
                    if (startButton != null) {
                        startButton.interactable = true;
                    }
                    if (setValueButton != null) {
                        setValueButton.interactable = true;
                    }
                    readingData = true;
                })
        );
    }

    private async void ConfigureSensorFusionAsync() {
        var cts = new CancellationTokenSource();
        var dataCal = await sensorFusion.Calibrate(cts.Token, 1000, (state) => {

        });
        
        //IMacro macro = metawear.GetModule<IMacro>();
        //macro.StartRecord();

        sensorFusion.WriteCalibrationData(dataCal);

        //var id = macro.EndRecordAsync();

        sensorFusion.EulerAngles.Start();
        sensorFusion.Start();
        SetState(States.ReadSensorFusion, 0.5f);
        //startButton.interactable = true;
        //setValueButton.interactable = true;
    }

    private async void ConnectAsync() {
        
        await metawear.InitializeAsync();
        metawear.OnUnexpectedDisconnect = () => { 
            StatusMessage = "Unexpectedly lost connection";
            Debug.Log("COOOOOOOOOOONNECTION LOST!!!!");
        };
        
        ILed led = metawear.GetModule<ILed>();
        led.EditPattern(MbientLab.MetaWear.Peripheral.Led.Color.Green, MbientLab.MetaWear.Peripheral.Led.Pattern.Blink, 0, 4);
        led.Play();

        SetState(States.ConfigureSensorFusion, 0.5f);
    }

    public void OnDisconnect () {
        if (connection) {
            Debug.Log("Wrong Disconnect!");
            SetState(States.Connect, 0.5f);
        }
        readingData = false;
    }

    public bool IsConnected () {
        return metawear != null ? metawear.IsConnected && readingData : false;
    }

}