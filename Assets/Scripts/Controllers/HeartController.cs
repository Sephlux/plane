﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeartController : MonoBehaviour {

    [SerializeField]
    private float scrollSpeed = 0.002f;
    [SerializeField]
    private GameManager gameManager;

    private Vector3 startPosition;

    private float deleteBound = -10.0f;

    private float speed;

    private Color32 newColor = new Color(255, 255, 255, 255);

    public float Speed { get => this.speed; set => this.speed = value; }
    public GameManager GameManager { get => this.gameManager; set => this.gameManager = value; }

    // Start is called before the first frame update
    void Start() {
        startPosition = transform.position;
        StartCoroutine(FadeOut(GetComponent<SpriteRenderer>()));
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (!GameManager.GameOver) {
            Vector2 newPosition = new Vector2(transform.position.x, transform.position.y + scrollSpeed * Speed * 0.75f);
            transform.position = Vector2.MoveTowards(transform.position, newPosition, scrollSpeed * Speed);
            if (transform.position.x <= deleteBound) {
                Destroy(this.gameObject);
            }

            if (transform.position.y <= deleteBound) {
                Destroy(this.gameObject);
            }
        }
    }

    private IEnumerator FadeOut(SpriteRenderer text) {
        float t = 0;
        Color32 startColor = new Color32(255, 255, 255, 255);
        Color32 endColor = new Color32(255, 255, 255, 0);

        text.color = startColor;

        while (t < 1) {
            text.color = Color.Lerp(startColor, endColor, t);
            t += Time.deltaTime * 1.5f;
            yield return null;
        }

    }

}
