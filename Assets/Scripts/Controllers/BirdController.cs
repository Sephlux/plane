﻿using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour
{

    [SerializeField]
    private float scrollSpeed = 0.001f;
    [SerializeField]
    private GameManager gameManager;
    private PlaneContoller planeContoller;

    private Vector2 savedOffset;
    private Vector3 startPosition;

    private float deleteBound = -10.0f;
    private bool collision = false;
    private bool played = false;
    private float speed;

    public float Speed { get => this.speed; set => this.speed = value; }
    public GameManager GameManager { get => this.gameManager; set => this.gameManager = value; }
    public PlaneContoller PlaneContoller { get => this.planeContoller; set => this.planeContoller = value; }

    void Start() {
        startPosition = transform.position;
    }

    void FixedUpdate() {
        if (!collision && !GameManager.GameOver) {
            Vector2 newPosition = new Vector2(transform.position.x - scrollSpeed*Speed, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, newPosition, scrollSpeed*Speed);

            if (transform.position.x <= deleteBound) {
                Destroy(this.gameObject);
                GameManager.IncreaseHighscore();
            }

            if (transform.position.y <= deleteBound) {
                Destroy(this.gameObject);
            }
        } else {
            this.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            if (!collision) {
                this.transform.GetComponent<PolygonCollider2D>().enabled = false;
            }
        }
    }

    /*void OnCollisionEnter2D(Collision2D col) {
        if ((col.gameObject.tag == "Plane" || col.gameObject.tag == "Bullet") && !planeContoller.Shield) {
            collision = true;
            this.transform.GetComponent<Animator>().SetTrigger("Crash");
            this.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            StartCoroutine(Die());
        }
    }*/

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Bullet") {
            collision = true;
            this.transform.GetComponent<Animator>().SetTrigger("Crash");
            this.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            StartCoroutine(Die());
        }
        if (col.gameObject.tag == "Plane" && !planeContoller.Shield && planeContoller.Hp == 3) {
            collision = true;
            this.transform.GetComponent<Animator>().SetTrigger("Crash");
            this.transform.GetChild(0).GetComponent<Animator>().enabled = false;
            StartCoroutine(Die());
        }
    }

    private IEnumerator Die() {
        yield return new WaitForSeconds(0.3f);
        this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        this.transform.GetComponent<PolygonCollider2D>().enabled = false;
    }
}