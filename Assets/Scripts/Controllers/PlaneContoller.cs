﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaneContoller : MonoBehaviour {

    [SerializeField]
    private GameManager gameManager;

    [SerializeField]
    private bool debug = false;

    [SerializeField]
    private Button shootButton;

    [SerializeField]
    private GameObject bulletPrefab;

    public bool Shoot { get; set; }

    public bool Shield { get; set; }

    private Animator animator;
    private Rigidbody2D rigidbodyPlain;
    private Vector3 wrld;
    private float half_sz;
    private float fillAmount = 1.0f;
    private float planeSpeed = 0.15f;
    private float force = 0.5f;
    private Image content;
    private int hp = 0;
    private float positionDelta = 0.04f;
    private float deltaTime = 0.0f;
    private GameObject oldCollission;
    private GameObject sensorObject;
    private SensorManager sensorManager;
    private SettingsData data;
    private float shootTime = 2.0f;
    private float shootDelta = 2.0f;

    float positiveRange, negativeRange;


    public int Hp { get => this.hp; set => this.hp = value; }


    // Start is called before the first frame update
    void Start() {
        animator = GetComponent<Animator>();
        rigidbodyPlain = GetComponent<Rigidbody2D>();
        wrld = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, 0.0f));
        half_sz = gameObject.GetComponent<Renderer>().bounds.size.y / 2;
        gameManager.SetPosition(this.transform);
        sensorObject = GameObject.FindGameObjectWithTag("SensorManager");
        sensorManager = sensorObject.GetComponent<SensorManager>();
        sensorManager.GameRunning = true;
        data = DataManager.Load();
        positiveRange = (-wrld.y) / (data.to-data.neutral);
        negativeRange = wrld.y / (data.from-data.neutral);
        planeSpeed = data.planeSpeed / 1000;
        GetComponent<AudioSource>().volume = 0.5f * data.soundEffectVolume / 100;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (!gameManager.GameOver) {
            checkBounds();
            handleSensorInput();
            sendPositionData();
            checkShoot();
            checkShield();
        } else {
            //rigidbodyPlain.velocity = Vector3.zero;
            //rigidbodyPlain.angularVelocity = 0.0f;
        }
    }

    private void checkShoot() {
        if(Shoot) {
            shootButton.image.enabled = true;
            shootDelta += Time.deltaTime;
            shootButton.image.fillAmount = shootDelta / shootTime;
            shootButton.gameObject.SetActive(true);
        } else {
            shootButton.image.enabled = false;
            shootButton.image.fillAmount = 0;
            shootDelta = 2.0f;
            shootButton.gameObject.SetActive(false);
        }
    }

    private void checkShield() {
        if (Shield) {
            Vector2 size = GetComponent<SpriteRenderer>().size;
            size.x += 1f;
            size.y += 1f;
            transform.GetChild(4).GetComponent<SpriteRenderer>().size = size;
            transform.GetChild(4).gameObject.SetActive(true);
        } else {
            transform.GetChild(4).gameObject.SetActive(false);
        }
    }

    private void handleInput() {
        if (Input.GetKey("up")) {
            this.transform.position = new Vector3(transform.position.x, transform.position.y + planeSpeed, transform.position.z);
        } else if (Input.GetKey("down")) {
            this.transform.position = new Vector3(transform.position.x, transform.position.y - planeSpeed, transform.position.z);
        } else {
            //rigidbodyPlain.velocity = Vector3.zero;
            //rigidbodyPlain.angularVelocity = 0.0f;
        }


    }

    private void sendPositionData() {
        deltaTime += Time.deltaTime;
        //if (deltaTime >= positionDelta) {
            deltaTime = 0;
            gameManager.SetPosition(this.transform);
        //}
    }

    private void handleSensorInput() {
        if (!debug) {
            float val = sensorManager.CurrentValue - data.neutral;
            //Debug.Log(val);
            float newY;

            if (val < 0) {
                //DOWN
                newY = negativeRange * val;
                if (this.transform.position.y > newY) {
                    this.transform.position = new Vector3(transform.position.x, transform.position.y - planeSpeed, transform.position.z);
                }
            } else {
                //UP
                newY = positiveRange * val;
                if (this.transform.position.y < newY) {
                    this.transform.position = new Vector3(transform.position.x, transform.position.y + planeSpeed, transform.position.z);
                }
            }
        } else {
            handleInput();
        }
    }

    private void checkBounds() {
        if (this.transform.position.y < (wrld.y + half_sz)) {
            this.transform.position = new Vector3(this.transform.position.x, (wrld.y + half_sz), 0.0f);
            rigidbodyPlain.velocity = Vector3.zero;
            rigidbodyPlain.angularVelocity = 0.0f;
        }

        if (this.transform.position.y > -(wrld.y + half_sz)) {
            this.transform.position = new Vector3(this.transform.position.x, -(wrld.y + half_sz), 0.0f);
            rigidbodyPlain.velocity = Vector3.zero;
            rigidbodyPlain.angularVelocity = 0.0f;
        }
    }

    public void ShootProjectile() {
        if (shootDelta > shootTime) {
            shootDelta = 0.0f;
            shootButton.image.fillAmount = 0;
            animator.SetTrigger("Shoot");
            for (int i = 0; i < 3; i++) {
                //GameObject obj = Instantiate(bulletPrefab, transform.GetChild(0).transform.position, transform.rotation); //.GetChild(0).transform
                //obj.transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
                StartCoroutine(ShootOne(i * 0.1f, transform.position.y));
            }
        }
    }

    /*void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.tag == "Bird" && !gameManager.GameOver && oldCollission != collision.gameObject) {
            if (!Shield) {
                Hp++;
                gameManager.LooseHP(Hp);
                GetComponent<Rigidbody2D>().gravityScale = 1;
                GetComponent<AudioSource>().Play();
                gameManager.GameOver = true;
                rigidbodyPlain.constraints = RigidbodyConstraints2D.FreezePositionX;
                animator.SetTrigger("Crash");
                sensorManager.GameRunning = false;
            } else {
                Shield = false;
                gameManager.RemovePowerUpTimer(PowerUp.SHIELD);
            }
        }
        oldCollission = collision.gameObject;
    }*/

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "BirdCollider" && oldCollission != collision.gameObject) {
            if (!Shield) {
                Hp++;
                gameManager.LooseHP(Hp);
                //Handheld.Vibrate();
                if (3-Hp == 0) {
                    /*GetComponent<PolygonCollider2D>().isTrigger = false;
                    GameObject[] birds = GameObject.FindGameObjectsWithTag("BirdCollider");
                    foreach (GameObject bird in birds) {
                        if (bird != collision.gameObject) {
                            bird.GetComponent<PolygonCollider2D>().isTrigger = false;
                        }
                    }
                    GameObject[] hearts = GameObject.FindGameObjectsWithTag("PowerUp");
                    foreach (GameObject heart in hearts) {
                        heart.GetComponent<PolygonCollider2D>().isTrigger = false;
                    }*/
                    GetComponent<Rigidbody2D>().gravityScale = 1;
                    GetComponent<AudioSource>().Play();
                    gameManager.GameOver = true;
                    rigidbodyPlain.constraints = RigidbodyConstraints2D.FreezePositionX;
                    animator.SetTrigger("Crash");
                    sensorManager.GameRunning = false;
                } else {
                    animator.SetTrigger("LooseHP");
                    GetComponent<AudioSource>().Play();
                }
            } else {
                transform.GetChild(4).GetComponent<Animator>().SetTrigger("Crash");
                //StartCoroutine(DestroyShield());
            }
        }
        oldCollission = collision.gameObject;
    }

    private IEnumerator DestroyShield() {
        yield return new WaitForSeconds(1.5f);
        Shield = false;
        gameManager.RemovePowerUpTimer(PowerUp.SHIELD);
        transform.GetChild(4).GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.49f);
    }

    private IEnumerator ShootOne(float delay, float y) {
        yield return new WaitForSeconds(delay);
        GameObject obj = Instantiate(bulletPrefab, new Vector3(transform.GetChild(0).transform.position.x, y, 1.0f), transform.rotation); //.GetChild(0).transform
        obj.GetComponent<AudioSource>().volume = 0.5f * data.soundEffectVolume / 100;
        obj.transform.localScale = new Vector3(0.5f, 0.5f, 1.0f);
    }

    private void HandleShootButton() {  
        if (fillAmount > content.fillAmount) {
            content.fillAmount += planeSpeed * 1.5f * Time.deltaTime; //Mathf.Lerp(content.fillAmount, fillAmount, Time.deltaTime * lerpSpeed);
        } else {
            content.fillAmount = fillAmount;
        }
    }

}
