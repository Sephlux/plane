﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

    public float scrollSpeed = -0.25f;

    private float deleteBound = 15.0f;
    private bool collision = false;


    void Start() {

    }

    void Update() {
        if (!collision) {
            Vector2 newPosition = new Vector2(transform.position.x + scrollSpeed, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, newPosition, scrollSpeed);

            if (transform.position.x >= deleteBound) {
                Destroy(this.gameObject);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "BirdCollider") {
            collision = true;
            GetComponent<Animator>().SetTrigger("Poof");
            GetComponent<PolygonCollider2D>().enabled = false;
            //Destroy(this.gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "BirdCollider") {
            collision = true;
            GetComponent<Animator>().SetTrigger("Poof");
            GetComponent<PolygonCollider2D>().enabled = false;
            //Destroy(this.gameObject);
        }
    }

    public void OnBulletPoofEnd() {
        Destroy(this.gameObject);
    }


}
