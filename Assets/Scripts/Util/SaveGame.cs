﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveGame {

    private int highscore;
    private int timeS;
    private int timeM;
    private float birdSpeed;
    private float planeSpeed;
    private float birdInterval;
    private float powerUpInterval;
    private List<float> yPosition;
    private DateTime date;
    private float from;
    private float to;


    public SaveGame(int score, int timeS, int timeM, float birdSpeed, float planeSpeed, float interval, List<float> yPosition, DateTime date, float from, float to, float powerUpInterval) {
        this.highscore = score;
        this.timeS = timeS;
        this.timeM = timeM;
        this.birdSpeed = birdSpeed;
        this.planeSpeed = planeSpeed;
        this.birdInterval = interval;
        this.powerUpInterval = powerUpInterval;
        this.yPosition = yPosition;
        this.date = date;
        this.from = from;
        this.to = to;
    }

    public int Highscore => this.highscore;

    public int TimeS => this.timeS;
    public int TimeM => this.timeM;

    public float BirdSpeed => this.birdSpeed;
    public float PlaneSpeed => this.planeSpeed;

    public float BirdInterval => this.birdInterval;

    public List<float> YPosition => this.yPosition;

    public DateTime Date { get => this.date; }
    public float From { get => this.from; }
    public float To { get => this.to; }
}
