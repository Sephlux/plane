﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SettingsData {
    /*
	public int[] highscores = { 0, 0, 0 };
	public List<int> shopBought = new List<int>(){0};
	public string username = null;
    public DateTime adTime = DateTime.MinValue;
    public bool adWatched = false;
    public int adOffset = 5;
    */

    public float gameSpeed = 100f;
    public float planeSpeed = 200f;
    public float birdInterval = 5f;
    public float powerUpInterval = 10f;
    public float from = -50f;
    public float to = 50f;
    public float neutral = 0f;
    public float maxScore = 0f;
    public float musicVolume = 50f;
    public bool debugMode = false;
    public float soundEffectVolume = 50f;
    public List<SaveGame> playedGames = new List<SaveGame>();
    public GameLevel gameLevel = GameLevel.SPACE;
}
