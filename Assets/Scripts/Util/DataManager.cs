﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DataManager {

	public static int score = 0;
	public static bool adReward = false;

	public static SettingsData Load(){
		if (File.Exists (Application.persistentDataPath + "/settings.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/settings.dat", FileMode.Open);
			SettingsData data = (SettingsData)bf.Deserialize (file);
			//if (data.shopSelection == null) {
				//data.shopSelection = new List<int>(){0, 1, 2, 3, 4, 5, 6};
				//data.buyCount = 6;
			//}
			file.Close ();
			return data;
		}
		return new SettingsData();
	}

	public static void Save(SettingsData data){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/settings.dat");

		bf.Serialize (file, data);
		file.Close ();
	}
}
