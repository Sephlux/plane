﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Net.Security;

public class Shoot : PowerUpController {

    private float shootTime = 10.0f;
    private bool shootFlag = false;
    private float shootDelta = 0.0f;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (shootFlag) {
            shootDelta += Time.deltaTime;
            Plane.GetComponent<PlaneContoller>().Shoot = true;
            shootFlag = true;
            if (shootDelta > shootTime) {
                shootFlag = false;
                Plane.GetComponent<PlaneContoller>().Shoot = false;
                Destroy(this.gameObject);
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col) {
        //base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Plane") {
            this.gameObject.transform.position = new Vector3(-10.0f, -10.0f, 0f);
            shootDelta = 0.0f;
            shootFlag = true;
            base.collision = true;
            Plane.GetComponent<PlaneContoller>().Shoot = true;
            GameManager.AddPowerUpTimer(PowerUp.SHOOT, shootTime);
        }
    }
    protected override void OnCollisionEnter2D(Collision2D col) {
        //base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Plane") {
            this.gameObject.transform.position = new Vector3(-10.0f, -10.0f, 0f);
            shootDelta = 0.0f;
            shootFlag = true;
            base.collision = true;
            Plane.GetComponent<PlaneContoller>().Shoot = true;
            GameManager.AddPowerUpTimer(PowerUp.SHOOT, shootTime);
        }
    }
}
