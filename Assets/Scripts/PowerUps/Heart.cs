﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : PowerUpController {

    [SerializeField]
    private GameObject heartPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Plane") {
            Plane.GetComponent<PlaneContoller>().Hp--;
            GameManager.AddHP(Plane.GetComponent<PlaneContoller>().Hp);
            Plane.GetComponent<PolygonCollider2D>().isTrigger = true;
            GameObject[] birds = GameObject.FindGameObjectsWithTag("BirdCollider");
            foreach (GameObject bird in birds) {
                bird.GetComponent<PolygonCollider2D>().isTrigger = false;
            }

            GameObject[] hearts = GameObject.FindGameObjectsWithTag("PowerUp");
            foreach (GameObject heart in hearts) {
                heart.GetComponent<PolygonCollider2D>().isTrigger = true;
            }
        }
        base.OnCollisionEnter2D(col);
    }
    protected override void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Plane") {
            if (Plane.GetComponent<PlaneContoller>().Hp > 0) {
                Plane.GetComponent<PlaneContoller>().Hp--;
                GameManager.AddHP(Plane.GetComponent<PlaneContoller>().Hp);
                GameObject obj = Instantiate(heartPrefab/*, GameObject.FindGameObjectWithTag("Plane").transform*/);
                obj.transform.position = this.transform.position;
                obj.GetComponent<HeartController>().Speed = base.Speed;
                obj.GetComponent<HeartController>().GameManager = base.GameManager;
            }
        }
        base.OnTriggerEnter2D(col);
    }
}
