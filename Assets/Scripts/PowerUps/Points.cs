﻿using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System.Net.Security;

public class Points : PowerUpController {

    [SerializeField]
    private GameObject scoreTextPrefab;

    // Start is called before the first frame update
    void Start() {

    }

    protected override void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.tag == "Plane") {
            base.collision = true;
            GameObject obj = Instantiate(scoreTextPrefab, GameObject.FindGameObjectWithTag("Canvas").transform);
            obj.transform.position = this.transform.position;
            obj.GetComponent<ScoreController>().Speed = base.Speed;
            obj.GetComponent<ScoreController>().GameManager = base.GameManager;
            GameManager.Highscore += 250;
            Destroy(this.gameObject);
        }
    }
}
