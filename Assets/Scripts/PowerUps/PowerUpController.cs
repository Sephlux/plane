﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PowerUp {
    HEART = 0, SHIELD = 1, SHOOT = 2, POINTS = 34
}

public class PowerUpController : MonoBehaviour
{
    [SerializeField]
    private float scrollSpeed = 0.002f;

    private GameManager gameManager;
    private GameObject plane;

    private Vector2 savedOffset;
    private Vector3 startPosition;

    private float deleteBound = -10.0f;
    protected bool collision = false;
    private bool played = false;
    private float speed;

    public float Speed { get => this.speed; set => this.speed = value; }
    public GameManager GameManager { get => this.gameManager; set => this.gameManager = value; }
    public GameObject Plane { get => this.plane; set => this.plane = value; }

    void Start() {
        startPosition = transform.position;
    }

    void FixedUpdate() {
        if (!collision && !GameManager.GameOver) {
            Vector2 newPosition = new Vector2(transform.position.x - scrollSpeed * Speed, transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, newPosition, scrollSpeed * Speed);

            if (transform.position.x <= deleteBound) {
                Destroy(this.gameObject);
                gameManager.PowerUps.Remove(this.gameObject);
            }

            if (transform.position.y <= deleteBound) {
                Destroy(this.gameObject);
                gameManager.PowerUps.Remove(this.gameObject);
            }
        }
    }

    protected virtual void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "Plane") {
            collision = true;
            Destroy(this.gameObject);
        }
    }
    protected virtual void OnTriggerEnter2D(Collider2D col) {
            if (col.gameObject.tag == "Plane") {
                collision = true;
                Destroy(this.gameObject);
            }
    }
}
