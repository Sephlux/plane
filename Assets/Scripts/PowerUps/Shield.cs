﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : PowerUpController {

    private float shieldTime = 10.0f;
    private float shieldDelta = 0.0f;
    private bool shieldFlag = false;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (shieldFlag) {
            shieldDelta += Time.deltaTime;
            Plane.GetComponent<PlaneContoller>().Shield = true;
            shieldFlag = true;
            if (shieldDelta > shieldTime) {
                shieldFlag = false;
                Plane.GetComponent<PlaneContoller>().Shield = false;
                Destroy(this.gameObject);
            }
        }
    }

    protected override void OnTriggerEnter2D(Collider2D col) {
        //base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Plane") {
            this.gameObject.transform.position = new Vector3(-10.0f, -10.0f, 0f);
            shieldFlag = true;
            base.collision = true;
            Plane.GetComponent<PlaneContoller>().Shield = true;
            GameManager.AddPowerUpTimer(PowerUp.SHIELD, shieldTime);
        }
    }
    protected override void OnCollisionEnter2D(Collision2D col) {
        //base.OnTriggerEnter2D(col);
        if (col.gameObject.tag == "Plane") {
            this.gameObject.transform.position = new Vector3(-10.0f, -10.0f, 0f);
            shieldDelta = 0.0f;
            shieldFlag = true;
            base.collision = true;
            Plane.GetComponent<PlaneContoller>().Shield = true;
            GameManager.AddPowerUpTimer(PowerUp.SHIELD, shieldTime);
        }
    }
}
