﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpTimer : MonoBehaviour {

    public float Timer { get; set; }
    public PowerUp PowerUp { get; set; }

    private float deltaTime = 0.0f;
    private Sprite powerUpIcon;
    public Sprite PowerUpIcon { get => this.powerUpIcon; set {
            this.powerUpIcon = value;
            this.transform.GetChild(0).GetComponent<Image>().sprite = value;
        }
    }

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void FixedUpdate() {
        deltaTime += Time.deltaTime;
        if (deltaTime < Timer) {
            this.transform.GetChild(1).GetComponent<Text>().text = (Timer - deltaTime).ToString("F0") + "s";
        } else {
            Destroy(this.gameObject);
        }
    }

    public void UpdateTime(PowerUp powerUp) {
        if (this.PowerUp == powerUp) {
            this.deltaTime = 0.0f;
        }
    }
}
