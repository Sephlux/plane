﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

    [SerializeField]
    private Sprite[] levelBackground;

    [SerializeField]
    private Sprite[] planeSprite;


    //Animator.SetLayerWeight

    public Sprite[] LevelBackground { get => this.levelBackground; set => this.levelBackground = value; }
    public Sprite[] PlaneSprite { get => this.planeSprite; set => this.planeSprite = value; }
}

public enum GameLevel {
    LAND = 0, WATER=1, SPACE = 2
}
