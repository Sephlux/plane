﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class SettingsManager : MonoBehaviour {

    [SerializeField]
    private Text speedSliderText;
    [SerializeField]
    private Text planeSliderText;
    [SerializeField]
    private Text birdSliderText;
    [SerializeField]
    private Text powerUpSliderText;
    [SerializeField]
    private GameObject testPanel;
    [SerializeField]
    private Slider birdSpeedSlider;
    [SerializeField]
    private Slider planeSpeedSlider;
    [SerializeField]
    private Slider birdSlider;
    [SerializeField]
    private Slider powerUpSlider;
    [SerializeField]
    private InputField fromInput;
    [SerializeField]
    private InputField toInput;
    [SerializeField]
    private Text toggleBtnText;

    private SettingsData data;


    [SerializeField]
    private Text Values;
    [SerializeField]
    private Text StatusText;
    [SerializeField]
    private Text connectText;
    [SerializeField]
    private GameObject foot;
    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Button setValueButton;
    [SerializeField]
    private Button connectButton;
    [SerializeField]
    private Button gameSettingsButton;
    [SerializeField]
    private Button sensorSettingsButton;
    [SerializeField]
    private Button generalSettingsButton;
    [SerializeField]
    private GameObject gameSettingsPanel;
    [SerializeField]
    private GameObject sensorSettingsPanel;
    [SerializeField]
    private GameObject generalSettingsPanel;

    [SerializeField]
    private Text musicSliderText;
    [SerializeField]
    private Slider musicSlider;
    [SerializeField]
    private Text effectSliderText;
    [SerializeField]
    private Slider effectSlider;
    [SerializeField]
    private Text debugToggleText;
    [SerializeField]
    private Toggle debugToggle;


    private SensorManager sensorManager;

    public float Neutral { get; set; }

    // Start is called before the first frame update
    void Start() {
        //SensorManager.MainAsync(null).Wait();
        data = DataManager.Load();
        birdSpeedSlider.GetComponent<Slider>().value = data.gameSpeed;
        planeSpeedSlider.GetComponent<Slider>().value = data.planeSpeed;
        birdSlider.GetComponent<Slider>().value = data.birdInterval;
        powerUpSlider.GetComponent<Slider>().value = data.powerUpInterval;
        musicSlider.GetComponent<Slider>().value = data.musicVolume;
        effectSlider.GetComponent<Slider>().value = data.soundEffectVolume;
        fromInput.text = data.from.ToString();
        toInput.text = data.to.ToString();
        debugToggle.isOn = data.debugMode;
        if (debugToggle.isOn) {
            debugToggleText.text = "<color=lime>On</color>";
        } else {
            debugToggleText.text = "<color=red>Off</color>";
        }
        speedSliderText.text = birdSpeedSlider.GetComponent<Slider>().value.ToString();
        planeSliderText.text = planeSpeedSlider.GetComponent<Slider>().value.ToString();
        birdSliderText.text = birdSlider.GetComponent<Slider>().value.ToString() + " sec";
        birdSliderText.text = powerUpSlider.GetComponent<Slider>().value.ToString() + " sec";
        musicSliderText.text = musicSlider.GetComponent<Slider>().value.ToString();
        effectSliderText.text = effectSlider.GetComponent<Slider>().value.ToString();

        OnChangeSpeed();
        OnChangePlaneSpeed();
        OnChangeBird();

        sensorManager = GameObject.FindGameObjectWithTag("SensorManager").GetComponent<SensorManager>();
        sensorManager.Values = Values;
        sensorManager.StatusText = StatusText;
        sensorManager.setValueButton = setValueButton;
        sensorManager.connectText = connectText;
        sensorManager.foot = foot;
        sensorManager.startButton = startButton;
        sensorManager.connectButton = connectButton;
        sensorManager.fromInput = fromInput;
        sensorManager.toInput = toInput;
        sensorManager.settingsManager = this;
        if (sensorManager.State != SensorManager.States.ReadSensorFusion) {
            connectButton.interactable = false;
            startButton.interactable = false;
            setValueButton.interactable = false;
        }

        sensorManager.startButton.onClick.AddListener(delegate { sensorManager.StartTest(); });
        sensorManager.setValueButton.onClick.AddListener(delegate { sensorManager.SetValues(); });
        sensorManager.connectButton.onClick.AddListener(delegate { sensorManager.Connect(); });

        StatusText.text = "Status: " + sensorManager.GetComponent<SensorManager>().StatusMessage;
    }

    // Update is called once per frame
    void Update() {

    }

    public void OnChangeSpeed() {
        speedSliderText.text = birdSpeedSlider.GetComponent<Slider>().value.ToString();
        if (birdSpeedSlider.GetComponent<Slider>().value <= 166) {
            speedSliderText.text += "  (<color=lime> easy </color>)";
        } else if (birdSpeedSlider.GetComponent<Slider>().value <= 166 * 2) {
            speedSliderText.text += "  (<color=yellow> medium </color>)";
        } else {
            speedSliderText.text += "  (<color=red> hard </color>)";
        }
    }

    public void OnChangePlaneSpeed() {
        planeSliderText.text = planeSpeedSlider.GetComponent<Slider>().value.ToString();
        if (planeSpeedSlider.GetComponent<Slider>().value <= 100 || planeSpeedSlider.GetComponent<Slider>().value > 400) {
            planeSliderText.text += "  (<color=red> hard </color>)";
        } else if (planeSpeedSlider.GetComponent<Slider>().value < 200 || planeSpeedSlider.GetComponent<Slider>().value > 300) {
            planeSliderText.text += "  (<color=yellow> medium </color>)";
        } else {
            planeSliderText.text += "  (<color=lime> easy </color>)";
        }
    }

    public void OnChangeBird() {
        birdSliderText.text = birdSlider.GetComponent<Slider>().value.ToString() + " sec";
        if (birdSlider.GetComponent<Slider>().value < 4) {
            birdSliderText.text += "  (<color=red> hard </color>)";
        } else if (birdSlider.GetComponent<Slider>().value < 7) {
            birdSliderText.text += "  (<color=yellow> medium </color>)";
        } else {
            birdSliderText.text += "  (<color=lime> easy </color>)";
        }
    }

    public void OnChangePowerUp() {
        powerUpSliderText.text = powerUpSlider.GetComponent<Slider>().value.ToString() + " sec";
        if (powerUpSlider.GetComponent<Slider>().value > 14) {
            powerUpSliderText.text += "  (<color=red> hard </color>)";
        } else if (powerUpSlider.GetComponent<Slider>().value > 7) {
            powerUpSliderText.text += "  (<color=yellow> medium </color>)";
        } else {
            powerUpSliderText.text += "  (<color=lime> easy </color>)";
        }
    }
    public void OnSave() {
        //TODO: Save values
        data.gameSpeed = birdSpeedSlider.value;
        data.planeSpeed = planeSpeedSlider.value;
        data.birdInterval = birdSlider.value;
        data.powerUpInterval = powerUpSlider.value;
        data.from = float.Parse(fromInput.text);
        data.to = float.Parse(toInput.text);
        data.neutral = Neutral;
        data.musicVolume = musicSlider.value;
        data.soundEffectVolume = effectSlider.value;
        data.debugMode = debugToggle.isOn ? true : false;
        DataManager.Save(data);
        // Debug.Log(data.neutral);
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }
    public void OnExit() {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }
    public void OnCloseTest() {
        testPanel.SetActive(false);
        toggleBtnText.text = "Start";
    }
    public void OnOpenTest() {
        testPanel.SetActive(true);
        //SceneManager.LoadScene("Example/MetaMotionR/MetaMotionR", LoadSceneMode.Single);
    }

    public void OnToggleTest() {
        if (toggleBtnText.text == "Start") {
            toggleBtnText.text = "Stop";
        } else {
            toggleBtnText.text = "Start";
        }
    }

    public void OnSensorSettingsButtonClick() {
        gameSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);
        sensorSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 45);
        generalSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);

        sensorSettingsButton.GetComponent<Image>().color = Color.white;
        gameSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
        generalSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);

        gameSettingsPanel.SetActive(false);
        sensorSettingsPanel.SetActive(true);
        generalSettingsPanel.SetActive(false);
    }

    public void OnGameSettingsButtonClick() {
        gameSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 45);
        sensorSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);
        generalSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);

        gameSettingsButton.GetComponent<Image>().color = Color.white;
        sensorSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
        generalSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
        
        gameSettingsPanel.SetActive(true);
        sensorSettingsPanel.SetActive(false);
        generalSettingsPanel.SetActive(false);
    }

    public void OnGeneralSettingsButtonClick() {
        generalSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 45);
        sensorSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);
        gameSettingsButton.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 40);

        generalSettingsButton.GetComponent<Image>().color = Color.white;
        sensorSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);
        gameSettingsButton.GetComponent<Image>().color = new Color(0.59f, 0.59f, 0.59f);

        gameSettingsPanel.SetActive(false);
        sensorSettingsPanel.SetActive(false);
        generalSettingsPanel.SetActive(true);
    }

    public void OnChangeMusic() {
       musicSliderText.text = musicSlider.GetComponent<Slider>().value.ToString();
       GameObject obj = GameObject.FindGameObjectWithTag("GameMusic");
       obj.GetComponent<AudioSource>().volume = 0.25f * musicSlider.GetComponent<Slider>().value / 100;
    }

    public void OnChangeSoundEffects() {
        effectSliderText.text = effectSlider.GetComponent<Slider>().value.ToString();
    }

    public void OnChangeDebugToggle() {
        if (debugToggle.isOn) {
            debugToggleText.text = "<color=lime>On</color>";
        } else {
            debugToggleText.text = "<color=red>Off</color>";
        }
    }
}
  