﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoredGamesUIManager : MonoBehaviour {

    [SerializeField]
    private GameObject scrollContent;

    [SerializeField]
    private GameObject scrollItem;

    [SerializeField]
    private GameObject graph;

    private SettingsData data;

    // Start is called before the first frame update
    void Start() {
        data = DataManager.Load();
        if (data.playedGames != null) {
            foreach (SaveGame game in data.playedGames) {
                GameObject obj = Instantiate(scrollItem, scrollContent.transform.position, Quaternion.identity);
                obj.transform.SetParent(scrollContent.transform);
                obj.transform.localScale = new Vector3(1, 1, 1);
                obj.GetComponent<SavedGameManager>().Highscore = game.Highscore;
                obj.GetComponent<SavedGameManager>().TimeS = game.TimeS;
                obj.GetComponent<SavedGameManager>().TimeM = game.TimeM;
                obj.GetComponent<SavedGameManager>().BirdSpeed = game.BirdSpeed;
                obj.GetComponent<SavedGameManager>().PlaneSpeed = game.PlaneSpeed;
                obj.GetComponent<SavedGameManager>().Interval = game.BirdInterval;
                obj.GetComponent<SavedGameManager>().From = game.From;
                obj.GetComponent<SavedGameManager>().To = game.To;
                obj.GetComponent<SavedGameManager>().Date = game.Date;
                obj.GetComponent<SavedGameManager>().YPosition = game.YPosition;
                obj.GetComponent<SavedGameManager>().Graph = graph;
            }
        }
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void OnHome() {
        SceneManager.LoadScene("MenuScene", LoadSceneMode.Single);
    }
}
