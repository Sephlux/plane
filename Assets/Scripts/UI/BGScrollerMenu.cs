﻿using UnityEngine;
using System.Collections;

public class BGScrollerMenu : MonoBehaviour
{
    public float scrollSpeed = 2.0f;
    public float tileSizeZ = 50000.0f;

    [SerializeField]
    private Sprite[] backgrounds;

    [SerializeField]
    private GameObject plane;

    private Vector3 startPosition;
    private GameLevel activeLevel;
    private SettingsData data;

    public GameLevel ActiveLevel { get => this.activeLevel; set { 
            this.activeLevel = value;
            this.GetComponent<SpriteRenderer>().sprite = backgrounds[(int)activeLevel];
            plane.GetComponent<Animator>().SetLayerWeight(1, 0);
            plane.GetComponent<Animator>().SetLayerWeight(2, 0);
            plane.GetComponent<Animator>().SetLayerWeight(3, 0);
            plane.GetComponent<Animator>().SetLayerWeight((int)activeLevel+1, 1);
        } 
    }

    void Start() {
        startPosition = transform.position;
        data = DataManager.Load();
        activeLevel = data.gameLevel;
        this.GetComponent<SpriteRenderer>().sprite = backgrounds[(int)activeLevel];
        plane.GetComponent<Animator>().SetLayerWeight(1, 0);
        plane.GetComponent<Animator>().SetLayerWeight(2, 0);
        plane.GetComponent<Animator>().SetLayerWeight(3, 0);
        plane.GetComponent<Animator>().SetLayerWeight((int)activeLevel + 1, 1);
    }

    void Update() {
            float newPosition = Mathf.Repeat(Time.time * scrollSpeed, tileSizeZ);
            transform.position = startPosition + Vector3.left * newPosition;
    }
}