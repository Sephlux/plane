﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using UnityEngine;

public class GraphManager : MonoBehaviour {

    private List<float> yPosition;
    private SimplestPlot SimplestPlotScript;

    public List<float> YPosition { get => this.yPosition; set {
            this.yPosition = value;
            StartCoroutine(InitGraph());
        }
    }

    public int Seconds { get; set; }
    public string Min { get; set; }
    public string Max { get; set; }

    // Start is called before the first frame update
    void Start() {
        SimplestPlotScript = transform.GetChild(2).GetComponent<SimplestPlot>();
        SimplestPlotScript.MyPlotType = SimplestPlot.PlotType.TimeSeries;
        SimplestPlotScript.BackGroundColor = new Color(0.1f, 0.1f, 0.1f, 0.4f);
    }

    // Update is called once per frame
    void Update() {
        SimplestPlotScript.UpdatePlot();
    }

    public void OnCloseGraph() {
        this.gameObject.SetActive(false);
        this.yPosition = null;
    }

    IEnumerator InitGraph() {
        yield return new WaitForSeconds(0.2f);
        SimplestPlotScript.SeriesPlotY = new List<SimplestPlot.SeriesClass>();
        SimplestPlotScript.SeriesPlotY.Add (new SimplestPlot.SeriesClass());
        yPosition.Insert(0, 0);
        yPosition.Insert(0, 5);
        yPosition.Insert(0, -5);
        SimplestPlotScript.SeriesPlotY[0].YValues = yPosition.ToArray();
        SimplestPlotScript.SeriesPlotX = generateXValues(yPosition.Count);

        SimplestPlotScript.SetYAxisText(Min, Max);

        SimplestPlotScript.UpdatePlot();
    }

    private float[] generateXValues(int count) {
        List<float> values = new List<float>();
        float interval = (float)Seconds / (float)count;
        values.Add(0);
        values.Add(0);
        values.Add(0);
        for (int i = 0; i < count; i++) {
            //values.Add(i * 0.04f);
            values.Add(i * interval);
        }
        return values.ToArray();
    }
}
