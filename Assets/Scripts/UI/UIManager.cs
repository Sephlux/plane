﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {


    // Start is called before the first frame update
    [SerializeField]
    private GameObject sensorManagerPrefab;

    [SerializeField]
    private GameObject levelSelection;

    [SerializeField]
    private Button startButton;

    [SerializeField]
    private Text statusText;

    [SerializeField]
    private Text bestScore;

    [SerializeField]
    private bool debug = false;

    [SerializeField]
    private AudioClip[] audios;


    private GameObject sensorManager;

    private SettingsData data;





    void Start() {
        sensorManager = GameObject.FindGameObjectWithTag("SensorManager");
        if (sensorManager == null) {
            sensorManager = Instantiate(sensorManagerPrefab);
            //sensorManager.GetComponent<SensorManager>().StatusText = statusText;
            //sensorManager.GetComponent<SensorManager>().Connect();
        }
        sensorManager.GetComponent<SensorManager>().StatusText = statusText;
        statusText.text = "Status: " + sensorManager.GetComponent<SensorManager>().StatusMessage;
        data = DataManager.Load();
        GameObject obj = GameObject.FindGameObjectWithTag("GameMusic");
        if (!obj.GetComponent<AudioSource>().isPlaying) {
            obj.GetComponent<AudioSource>().volume = 0.25f * data.musicVolume / 100;
            obj.GetComponent<AudioSource>().clip = audios[(int)data.gameLevel];
            obj.GetComponent<AudioSource>().Play();
        }
    }

    // Update is called once per frame
    void Update() {
        //Debug.Log(GameObject.FindGameObjectWithTag("SensorManager").GetComponent<SensorManager>().CurrentValue);
        if (sensorManager != null) {
            if (!sensorManager.GetComponent<SensorManager>().IsConnected() && !debug) {
                startButton.interactable = false;
            } else {
                startButton.interactable = true;
            }
            if (debug) {
                statusText.text = "Status: Debug Mode";
            }
        } else {
            sensorManager = Instantiate(sensorManagerPrefab);
            sensorManager.GetComponent<SensorManager>().StatusText = statusText;
            statusText.text = sensorManager.GetComponent<SensorManager>().StatusMessage;
            //sensorManager.GetComponent<SensorManager>().Connect();
        }
    }

    public void StartGame() {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void LoadSettings() {
        SceneManager.LoadScene("SettingsScene", LoadSceneMode.Single);
    }

    public void LoadSavedGames() {
        SceneManager.LoadScene("StoredGamesScene", LoadSceneMode.Single);
    }


    public void CloseLevelSelection() {
        levelSelection.SetActive(false);
    }

    public void OpenLevelSelection() {
        levelSelection.SetActive(true);
        bestScore.text = "Best Score: " + ((int)data.maxScore);
    }


}
