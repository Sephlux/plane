﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class SavedGameManager : MonoBehaviour {

    private int highscore;
    private int timeS;
    private int timeM;
    private float birdSpeed;
    private float planeSpeed;
    private float interval;
    private List<float> yPosition;
    private GameObject graph;
    private DateTime date;
    private float from;
    private float to;

    public DateTime Date {
        get => this.date;
        set {
            this.date = value;
            this.transform.GetChild(0).GetComponent<Text>().text = this.date.ToString();
        }
    }
    public int Highscore {
        get => this.highscore;
        set {
            this.highscore = value;
            this.transform.GetChild(1).GetComponent<Text>().text = "Score: " + this.highscore.ToString();
        }
    }
    public int TimeS {
        get => this.timeS; set {
            this.timeS = value;
            this.transform.GetChild(2).GetComponent<Text>().text = "Time: " + this.timeM.ToString() + "m " + this.timeS.ToString() + "s";
        }
    }
    public int TimeM {
        get => this.timeM; set {
            this.timeM = value;
            this.transform.GetChild(2).GetComponent<Text>().text = "Time: " + this.timeM.ToString() + "m " + this.timeS.ToString() + "s";
        }
    }
    public List<float> YPosition {
        get => this.yPosition; set {
            this.yPosition = value;

        }
    }
    public float BirdSpeed {
        get => this.birdSpeed; set {
            this.birdSpeed = value;
            this.transform.GetChild(4).GetComponent<Text>().text = "Bird Speed: " + this.birdSpeed.ToString();
        }
    }
    public float PlaneSpeed {
        get => this.planeSpeed; set {
            this.planeSpeed = value;
            this.transform.GetChild(5).GetComponent<Text>().text = "Plane Speed: " + this.planeSpeed.ToString();
        }
    }
    public float Interval {
        get => this.interval; set {
            this.interval = value;
            this.transform.GetChild(6).GetComponent<Text>().text = "Interval: " + this.interval.ToString() + "s";
        }
    }
    public float From {
        get => this.from; set {
            this.from = value;
            this.transform.GetChild(7).GetComponent<Text>().text = "From: " + this.from + "\nTo: " + this.to;
        } 
    }
    public float To {
        get => this.to; set {
            this.to = value;
            this.transform.GetChild(7).GetComponent<Text>().text = "From: " + this.from + "\nTo: " + this.to;
        }
    }

    public GameObject Graph { get => this.graph; set => this.graph = value; }

    public void OnShowGraph() {
        Graph.SetActive(true);
        Graph.GetComponent<GraphManager>().Seconds = TimeM * 60 + TimeS;
        Graph.GetComponent<GraphManager>().YPosition = yPosition;
        Graph.GetComponent<GraphManager>().Min = From.ToString() + "°";
        Graph.GetComponent<GraphManager>().Max = To.ToString() + "°";
    }

}
