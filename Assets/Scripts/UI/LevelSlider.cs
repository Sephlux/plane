﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSlider : MonoBehaviour {

    private RectTransform parentRect;
    private int clickCount = 1;
    private SettingsData data;

    [SerializeField]
    private GameObject imagePrefab;
    [SerializeField]
    private GameObject nextLevelButton;
    [SerializeField]
    private GameObject previousLevelButton;
    [SerializeField]
    private Sprite[] levelSprites;
    [SerializeField]
    private GameObject parent;
    [SerializeField]
    private GameObject background;
    [SerializeField]
    private GameObject onlockOverlay;
    [SerializeField]
    private Button saveButton;
    [SerializeField]
    private AudioClip[] audios;

    // Start is called before the first frame update
    void Start() {
        parentRect = parent.GetComponent<RectTransform>();
        LoadImages();
        data = DataManager.Load();

        int selectedLevel = (int)data.gameLevel;
        float imageWidth = imagePrefab.transform.GetComponent<RectTransform>().sizeDelta.x + 25;
        Vector2 pos = new Vector2(parentRect.anchoredPosition.x - imageWidth * selectedLevel, parentRect.anchoredPosition.y);
        parentRect.anchoredPosition = pos;
        clickCount = selectedLevel + 1;
        if (clickCount == 3 && data.maxScore < 10000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 10000</color>";
        } else if (clickCount == 2 && data.maxScore < 5000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 5000</color>";
        } else {
            saveButton.interactable = true;
            onlockOverlay.SetActive(false);
        }
        if (clickCount == 3) {
            nextLevelButton.GetComponent<Button>().interactable = false;
        }
        if (clickCount == 1) {
            previousLevelButton.GetComponent<Button>().interactable = false;
        }

    }

    // Update is called once per frame
    void Update() {
        
    }


    private void LoadImages() {
        float imageWidth = imagePrefab.transform.GetComponent<RectTransform>().sizeDelta.x;
        float width = levelSprites.Length * imageWidth + (levelSprites.Length - 1) * 25;
        parentRect.sizeDelta = new Vector2(width, imageWidth);
        parentRect.anchoredPosition = new Vector2((width - imageWidth) / 2, 0);

        foreach (Sprite s in levelSprites) {
            GameObject obj = Instantiate(imagePrefab);
            obj.GetComponent<Image>().sprite = s;
            obj.transform.SetParent(parent.transform, false);
        }

    }

    IEnumerator LerpImage(Vector2 endPosition, RectTransform rect) {
        float timeOfTravel = 0.5f; //time after object reach a target place 
        float currentTime = 0; // actual floting time 
        float normalizedValue = 0;

        while (currentTime <= timeOfTravel) {
            currentTime += Time.deltaTime;
            normalizedValue = currentTime / timeOfTravel; // we normalize our time 

            rect.anchoredPosition = Vector3.Lerp(rect.anchoredPosition, endPosition, normalizedValue);
            yield return null;
        }
        if (clickCount < levelSprites.Length) {
            nextLevelButton.GetComponent<Button>().interactable = true;
        }
        if (clickCount > 1) {
            previousLevelButton.GetComponent<Button>().interactable = true;
        }
    }

    public void NextMap() {
        float imageWidth = imagePrefab.transform.GetComponent<RectTransform>().sizeDelta.x + 25;
        //mapScript.MapIndex = clickCount;
        clickCount++;
        if (clickCount == levelSprites.Length) {
            nextLevelButton.GetComponent<Button>().interactable = false;
        }

        if (clickCount == levelSprites.Length && data.maxScore < 10000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 10000</color>";
        } else if (clickCount == levelSprites.Length-1 && data.maxScore < 5000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 5000</color>";
        } else {
            onlockOverlay.SetActive(false);
            saveButton.interactable = true;
            data.gameLevel = (GameLevel)(clickCount - 1);
            DataManager.Save(data);
            background.GetComponent<BGScrollerMenu>().ActiveLevel = data.gameLevel;
            GameObject obj = GameObject.FindGameObjectWithTag("GameMusic");
            obj.GetComponent<AudioSource>().volume = 0.25f * data.musicVolume / 100;
            if (obj.GetComponent<AudioSource>().clip != audios[(int)data.gameLevel]) {
                obj.GetComponent<AudioSource>().Stop();
                obj.GetComponent<AudioSource>().clip = audios[(int)data.gameLevel];
                obj.GetComponent<AudioSource>().Play();
            }
        }

        previousLevelButton.SetActive(true);
        nextLevelButton.GetComponent<Button>().interactable = false;
        Vector2 pos = new Vector2(parentRect.anchoredPosition.x - imageWidth, parentRect.anchoredPosition.y);
        StartCoroutine(LerpImage(pos, parentRect));

    }

    public void PrevMap() {
        float imageWidth = imagePrefab.transform.GetComponent<RectTransform>().sizeDelta.x + 25;
        //mapScript.MapIndex = clickCount;
        clickCount--;
        if (clickCount == 1) {
            previousLevelButton.GetComponent<Button>().interactable = false;
        }

        if (clickCount == 3 && data.maxScore < 10000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 10000</color>";
        } else if (clickCount == 2 && data.maxScore < 5000) {
            onlockOverlay.SetActive(true);
            saveButton.interactable = false;
            onlockOverlay.transform.GetChild(0).GetComponent<Text>().text = "<color=\"red\">unlock with score > 5000</color>";
        } else {
            onlockOverlay.SetActive(false);
            saveButton.interactable = true;
            data.gameLevel = (GameLevel)(clickCount - 1);
            DataManager.Save(data);
            background.GetComponent<BGScrollerMenu>().ActiveLevel = data.gameLevel;
            GameObject obj = GameObject.FindGameObjectWithTag("GameMusic");
            obj.GetComponent<AudioSource>().volume = 0.25f * data.musicVolume / 100;
            if (obj.GetComponent<AudioSource>().clip != audios[(int)data.gameLevel]) {
                obj.GetComponent<AudioSource>().Stop();
                obj.GetComponent<AudioSource>().clip = audios[(int)data.gameLevel];
                obj.GetComponent<AudioSource>().Play();
            }
        }

        nextLevelButton.SetActive(true);
        previousLevelButton.GetComponent<Button>().interactable = false;
        Vector2 pos = new Vector2(parentRect.anchoredPosition.x + imageWidth, parentRect.anchoredPosition.y);
        StartCoroutine(LerpImage(pos, parentRect));

    }
}
